# Zdrojowa Hotels: Media Module

Media library

## NPM Required:

1. "tui-image-editor": "3.9.0"

2. "cropperjs": "1.5.6"

## Instalation

in composer:


    "repositories":[
        {
            "type": "vcs",
            "url" : "https://bitbucket.org/zdrojowainvest/mediamodule.git"
        }
    ],
    
    "config": {
        "optimize-autoloader": true,
        "preferred-install": "dist",
        "sort-packages": true,
        "bitbucket-oauth": {

        }
    },

Oauth in Bitbucket: https://confluence.atlassian.com/bitbucket/oauth-on-bitbucket-cloud-238027431.html

in webpack.mix.js

    mix.module('MediaManager', 'vendor/zdrojowainvest/mediamodule');

Add provider to config/app.php

    \Selene\Modules\MediaManager\Providers\MediaModuleServiceProvider::class,

Publish config: 

    php artisan vendor:publish --tag=config

Change URL to MediaModuleMicroService in config/mediamodule.php
