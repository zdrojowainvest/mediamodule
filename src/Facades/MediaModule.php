<?php

namespace Selene\Modules\MediaManager\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * @method static getImageEditorUrl(): String
 */
class MediaModule extends Facade
{
    /**
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'mediamodule';
    }

}
