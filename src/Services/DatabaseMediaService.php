<?php

namespace Selene\Modules\MediaManager\Services;

use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Selene\Modules\MediaManager\Models\Media;
use Selene\Modules\MediaManager\Models\MediaType;

class DatabaseMediaService implements MediaService
{

    public function upload(UploadedFile $file): Media
    {
        $path = $file->store($this->getUploadPath(), ['disk' => 'public']);

        return Media::create(
            [
                'path' => $path,
                'fullPath' => Storage::disk('public')->path($path),
                'file' => basename($path),
                'extension' => $file->extension(),
                'mimetype' => $file->getMimeType(),
                'size' => $file->getSize(),
            ]);
    }

    public function createDashboardThumbnail(Media $media): MediaType
    {
        $img = Image::make($media->fullPath)->heighten(300);

        return new MediaType('dashboardthumbnail', $img);
    }

    private function getUploadPath()
    {
        return 'media/' . now()->format('Y/m');
    }

    public function resize(UploadedFile $file, int $width, int $height): \Intervention\Image\Image
    {
        return Image::make($file)->resize($width, $height);
    }

    public function convertToBase64(\Intervention\Image\Image $image): string
    {
        return $image->encode('data-url');
    }

    public function cropImage(\Intervention\Image\Image $image, int $cropWidth, int $cropHeight, int $x, int $y): \Intervention\Image\Image
    {
        return $image->crop($cropWidth, $cropHeight, $x, $y);
    }

    public function createMediaTypeFromImage(\Intervention\Image\Image $image, string $name): MediaType
    {
        return new MediaType($name, $image);
    }

}
