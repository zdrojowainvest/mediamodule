<?php

namespace Selene\Modules\MediaManager\Services;

use Illuminate\Http\UploadedFile;
use Intervention\Image\Image;
use Selene\Modules\MediaManager\Models\Media;
use Selene\Modules\MediaManager\Models\MediaType;

interface MediaService
{
    public function upload(UploadedFile $file);

    public function createDashboardThumbnail(Media $media): MediaType;

    public function resize(UploadedFile $file, int $width, int $height): Image;

    public function convertToBase64(Image $image): string;

    public function cropImage(Image $image, int $cropWidth, int $cropHeight, int $x, int $y): Image;

    public function createMediaTypeFromImage(Image $image, string $name): MediaType;
}
