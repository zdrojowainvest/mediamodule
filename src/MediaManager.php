<?php
namespace Selene\Modules\MediaManager;

use Illuminate\Support\Facades\Config;
use Selene\Modules\Module;

class MediaManager extends Module
{
    public $imageEditorUrl;

    public function __construct()
    {
        $this->imageEditorUrl = Config::get('mediamodule.imageEditorUrl');
    }

    /**
     * @return string
     */
    public function getImageEditorUrl() {
        return $this->imageEditorUrl;
    }
}
