<?php

namespace Selene\Modules\MediaManager\Contracts;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\RedirectResponse;

interface MediaModuleContract
{
    /**
     * @return string
     */
    public function getImageEditorUrl();
}
