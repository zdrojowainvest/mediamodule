<?php

namespace Selene\Modules\MediaManager\Models;

use Jenssegers\Mongodb\Eloquent\Model;

class Media extends Model
{
    protected $connection = 'mongodb';

    protected $attributes = [
      'alt' => '',
      'title' => '',
      'description' => ''
    ];
    protected $fillable = [
        'file',
        'path',
        'fullPath',
        'extension',
        'mimetype',
        'size',
        'alt',
        'title',
        'description'
    ];

    public function addType(MediaType $mediaType)
    {
        if (!$this->types) {
            $this->types = [];
        }

        $newTypes = $this->types;

        $newTypes[$mediaType->name()] = (object) $mediaType->toArray();

        $this->types = $newTypes;
        $mediaType->save();
        $this->save();
    }

    private function defaultTypes()
    {
        return new \stdClass();
    }


}
