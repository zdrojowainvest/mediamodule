<?php

namespace Selene\Modules\MediaManager\Http\Controllers;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Intervention\Image\Facades\Image as ImageFacade;
use Intervention\Image\Image;
use MongoDB\BSON\Regex;
use MongoDB\Database;
use Psr\Http\Message\ResponseInterface;
use Selene\Modules\MediaManager\Http\Requests\ImageResizeRequest;
use Selene\Modules\MediaManager\Http\Requests\UpdateAttributeRequest;
use Selene\Modules\MediaManager\Models\Media;
use Selene\Modules\MediaManager\Services\DatabaseMediaService;
use Symfony\Component\VarDumper\Cloner\Data;

class MediaController extends Controller
{
    public function index() {
        return view("MediaManager::index", ['files' => Media::orderByDesc('created_at')->get()]);
    }

    public function ajaxUpload(Request $request, DatabaseMediaService $databaseMediaService) {
        ini_set('memory_limit', '2048M');
        $media = $databaseMediaService->upload($request->file('file'));
        if($media->mimetype === 'image/jpg' || $media->mimetype === 'image/png' || $media->mimetype === 'image/jpeg' ) {
            $media->addType($databaseMediaService->createDashboardThumbnail($media));
        }

        return response()->json([
            'src' => route('MediaManager::media.imageType', ['media' => $media->id, 'type' => 'dashboardthumbnail']),
            'id' => $media->id
        ]);
    }

    public function imageType(Media $media, string $mediatype) {
        if($media->extension === 'svg') return response(file_get_contents($media->fullPath))->header('Content-Type', 'image/svg+xml');

        if($media->types && $media->types[$mediatype]) {
            $image = ImageFacade::make($media->types[$mediatype]['fullPath']);
            return $image->response();
        }

        abort(404);
    }

    public function image(Media $media) {
        if($media->extension === 'svg') {
            return response(file_get_contents($media->fullPath))->header('Content-Type', 'image/svg+xml');
        }

        if(strpos($media->mimetype, 'image')) {
            $image = ImageFacade::make($media->fullPath);

            return $image->response();
        }

        return response()->file($media->fullPath);
    }


    public function info(Media $media) {
        return response()->json($media);
    }


    public function editorSave(Request $request, DatabaseMediaService $databaseMediaService) {
        $image = ImageFacade::make($request->image);
        $mediaType = $databaseMediaService->createMediaTypeFromImage($image, $request->name);
        $media = Media::find($request->media);
        $mediaType->setNamePrefix(basename($media->file));

        $media->addType($mediaType);

        return response()->json(['status' => 'success']);
    }

    public function updateInfo(UpdateAttributeRequest $request, Media $id) {
        $id->update([$request->attribute => (string) $request->value]);

        return response()->json(['status' => 'success']);
    }

    public function selector(Request $request, Media $media) {
        if($request->extensions === 'all') {
            return response()->json($media->select('_id', 'extension', 'mimetype', 'title', 'created_at')->orderByDesc('created_at')->get());
        } else {
            return response()->json(
                $media->select('_id', 'extension', 'mimetype', 'title', 'created_at')->whereIn('extension', explode(',', $request->query('extensions')))->orderByDesc('created_at')->get()
            );
        }
    }
}
