<?php

namespace Selene\Modules\MediaManager\Http\Controllers;

use Illuminate\Routing\Controller;
use Selene\Modules\MediaManager\Models\Media;

class ApiController extends Controller
{
    public function get(Media $media, string $mediatype = null) {
        return response()->json($media);
    }
}
