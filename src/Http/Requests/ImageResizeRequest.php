<?php

namespace Selene\Modules\MediaManager\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ImageResizeRequest extends FormRequest
{
    public function rules() {
        return [
            'image' => 'required|image'
        ];
    }
}
