<?php

namespace Selene\Modules\MediaManager\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateAttributeRequest extends FormRequest
{

    public function rules() {
        return [
            'attribute' => 'required|in:alt,description,title'
        ];
    }
}
