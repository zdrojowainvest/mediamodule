<?php

namespace Selene\Modules\MediaManager\Providers;

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\ServiceProvider;
use Selene\Modules\MediaManager\Contracts\MediaModuleContract;
use Selene\Modules\MediaManager\MediaManager;

class MediaModuleServiceProvider extends ServiceProvider
{

    public array $bindings = [
        MediaModuleContract::class => MediaManager::class,
    ];

    public array $singletons = [
        'mediamodule' => MediaModuleContract::class,
    ];

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->publishes([
            __DIR__ . '/../Config/mediamodule.php' => config_path('mediamodule.php'),
        ], 'config');
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->mergeConfigFrom(__DIR__ . '/../Config/mediamodule.php', 'mediamodule');
    }
}
