@extends('DashboardModule::dashboard.index')

@section('content')
    <div class="content-wrapper">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header clearfix">
                        <h4 class="card-title float-left">Media</h4>
                        <div class="float-right">
                            <button class="btn btn-success add-button" data-toggle="collapse" data-target=".collapse-upload"  aria-expanded="false">Dodaj</button>
                            <button class="btn xd btn-primary">Szukaj</button>
                        </div>
                    </div>
                    <div class="card-body media-background-color">

                        @include('MediaManager::partials.dropToUpload')

                        <div class="media">
                            @foreach($files as $file)
                                <div class="thumbnail" data-media-id="{{$file->_id}}">
                                    @if(strpos($file->mimetype, 'image') === 0)
                                        <img class="thumbnail__image lazy" data-src="{{route('MediaManager::media.imageType', ['media' => $file->id, 'type' => 'dashboardthumbnail'])}}" alt="">
                                    @elseif($file->mimetype === 'application/pdf')
                                        <i class="mdi mdi-file-pdf-outline"></i>
                                    @elseif(strpos($file->mimetype, 'video') === 0)
                                        <i class="mdi mdi-file-video-outline"></i>
                                    @else
                                        <i class="mdi mdi-file-outline"></i>
                                    @endif
                                    <span>{{ $file->title ?? '' }}</span>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('stylesheets')
    @parent
    <link rel="stylesheet" href="{{ mix('vendor/css/MediaManager.css') }}">
@endsection

@section('javascripts')
    @parent
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/imgareaselect/0.9.10/css/imgareaselect-default.css" integrity="sha256-2m75JSbDAbDaM0OVP9exQ69ZJjSRM3t5YYbP6RNuKPc=" crossorigin="anonymous" />
    @javascript('csrf', csrf_token())
    @javascript('ajaxUpload', route('MediaManager::media.upload.ajax'))
    @javascript('infoUrl', route('MediaManager::media.image.info', ['media' => '%%id%%']))
    @javascript('imageUrl', route('MediaManager::media.image', ['media' => '%%id%%']))
    @javascript('imageUrlType', route('MediaManager::media.imageType', ['media' => '%%id%%', 'type' => '%%type%%']))
    @javascript('imageEditSaveUrl', route('MediaManager::media.image.editor.save'))
    @javascript('imageInfoUpdate', route('MediaManager::media.image.info.update', ['id' => '%%id%%']))
    @javascript('imageEditorUrl', config('mediamodule.imageEditorUrl'))
    <script src="{{ mix("vendor/js/MediaManager.js") }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/vanilla-lazyload@13.0.1/dist/lazyload.min.js"></script>


    <script>
        var lazyLoadInstance = new LazyLoad({
            elements_selector: ".lazy"
            // ... more custom settings?
        });
    </script>
    <script>
        $(function () {
            $(".xd").mediaSelector({
                extensions: 'all'
            });

            $(".xd").on('media:selected', function(event, args) {
                console.log(args);
            })
        })
    </script>
@endsection
