import modalTemplate from "./modalTemplate";
const ImageEditor = require('tui-image-editor');
import tuiTheme from "./tuiTheme";
import css from "tui-image-editor/dist/tui-image-editor.css";
const moment = require('./moment');

export default (id) => {
    const ajaxUrl = window['infoUrl'].split('%%id%%').join(id);

    $.ajax({
        url: ajaxUrl,
        method: 'GET',
        success: function (res) {
            window['canDrop'] = false;
            moment.locale('pl');

            let options = ``;

            if (res.mimetype === 'image/jpg' || res.mimetype === 'image/png' || res.mimetype === 'image/jpeg') {
                Object.keys(res.types).forEach((type) => {
                    options += `<option value="${window['imageUrlType'].split('%%id%%').join(res._id).split('%%type%%').join(type)}">${type}</option>`;
                });

                const template = modalTemplate.split('%%options%%').join(options).split('%%src%%').join(window['imageUrl'].split('%%id%%').join(res._id)).split('%%id%%').join(res._id).split('%%mime%%').join(res.mimetype).split('%%title%%').join(res.title).split('%%size%%').join(bytesToSize(res.size)).split('%%description%%').join(res.description).split('%%created%%').join(moment(res.created_at).format('LL')).split('%%alt%%').join(res.alt);

                $("body").append(template);

                $(".image-chooser").select2();

                $.ajax({
                    method: 'GET',
                    url: window['imageEditorUrl'] + 'status',
                    error: function() {
                        $(".edit-image").remove();
                    }
                });

            } else {
                let template = modalTemplate.split('%%options%%').join(options).split('%%id%%').join(res._id).split('%%mime%%').join(res.mimetype).split('%%title%%').join(res.title).split('%%size%%').join(bytesToSize(res.size)).split('%%description%%').join(res.description).split('%%created%%').join(moment(res.created_at).format('LL')).split('%%alt%%').join(res.alt);

                if(res.mimetype === 'application/pdf') {
                    template = template.split('<img src="%%src%%" alt="">').join('<i class="mdi mdi-file-pdf-outline"></i>');
                } else {
                    if(res.mimetype.startsWith('video/')) {
                        template = template.split('<img src="%%src%%" alt="">').join('<i class="mdi mdi-file-video-outline"></i>');
                    } else {
                        template = template.split('<img src="%%src%%" alt="">').join('<i class="mdi mdi-file-outline"></i>');
                    }
                }
                template = template.split('%%src%%').join(window['imageUrl'].split('%%id%%').join(res._id));

                $("body").append(template);

                $(".toolbar").remove();
                $(".container__image-padding img").remove();
                $(".edit-image").remove();
            }

            $(".media-info-input").keyup(debounce(function () {
                save($(this).data('id'), $(this).data('attr'), $(this).val());
            }, 300));

            $('.btn-save').click(function() {
                save($(this).data('id'), 'alt', $('input[data-attr="alt"]').val());
                save($(this).data('id'), 'title', $('input[data-attr="title"]').val());
                save($(this).data('id'), 'description', $('textarea[data-attr="description"]').val());
            });
        }
    })

    function save(id, attr, val) {
        $.ajax({
            method: 'PUT',
            url: window['imageInfoUpdate'].split('%%id%%').join(id),
            data: {
                _token: window['csrf'],
                attribute: attr,
                value: val
            }
        })
    }
}

function debounce(func, wait, immediate) {
    var timeout;
    return function() {
        var context = this, args = arguments;
        var later = function() {
            timeout = null;
            if (!immediate) func.apply(context, args);
        };
        var callNow = immediate && !timeout;
        clearTimeout(timeout);
        timeout = setTimeout(later, wait);
        if (callNow) func.apply(context, args);
    };
}

function bytesToSize(bytes) {
    var sizes = ['Bitów', 'KB', 'MB', 'GB', 'TB'];
    if (bytes == 0) return '0 Bitów';
    var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
    return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
}

$("body").on("click", '.media-modal', function (e) {
    if(e.target !== $(this)[0]) return;
    if(window['imageEditor']) {
        window['imageEditor'].close();
    }
   $(this).remove();
})

$("body").on('click', '.modal__close', function() {
    if(window['imageEditor']) {
        window['imageEditor'].close();
    }
    $('.media-modal').remove();
})
