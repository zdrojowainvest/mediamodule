import ImageEditor from './imageEditor/ImageEditor';

window['instance'] = null;
$("body").on('click', '.edit-image', function () {
    $("#image-editor").show();


    window['imageEditor'] = new ImageEditor('#image-editor', {
        image: $(".image-chooser").val(),
        mediaId: $(this).data('id')
    });
});

$("body").on('select2:select', '.image-chooser', function(e) {
    var data = e.params.data;
    $(".container__image-padding").find('img').eq(0).attr('src', data.id);
    $('.media-url-info').val(data.id);
})
