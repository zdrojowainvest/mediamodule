export default `
<div class="media-modal">
        <div class="media-modal__container">
            <div class="container__header">
                <div class="header__title">Szczegóły załączonego pliku</div>
                <div class="modal__close">
                <i class="mdi mdi-close"></i>
</div>
            </div>
            <div class="container__content">
                <div id="image-editor"></div>

                <div class="container__image">
                    <div class="toolbar">
                        <div class="chooser">
                            <p>Wybierz obraz</p>
                            <select name="" id="" class="image-chooser">
                                <option value="%%src%%" selected>Domyślny</option>
                                %%options%%
                            </select>
                        </div>
                    </div>
                    <div class="container__image-padding">
                        <img src="%%src%%" alt="">
                    </div>
                    <div class="edit-image" data-src="%%src%%" data-id="%%id%%">Edytuj obraz</div>
                </div>
                <div class="container__info">
                    <div class="info__static">
                        <p><strong>Identyfikator pliku: </strong>%%id%%</p>
                        <p><strong>Typ pliku: </strong>%%mime%%</p>
                        <p><strong>Wysłany na serwer: </strong>%%created%%</p>
                        <p><strong>Rozmiar pliku: </strong>%%size%%</p>
                    </div>
                    <div class="info__dynamic">
                        <div class="form-group">
                            <label class="col-sm-4 col-form-label">Tekst alternatywny</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control media-info-input" data-id="%%id%%" data-attr="alt" placeholder="Tekst alternatywny" value="%%alt%%">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 col-form-label">Tytuł</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control media-info-input" data-id="%%id%%" data-attr="title" placeholder="Tytuł" value="%%title%%">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 col-form-label">Opis</label>
                            <div class="col-sm-8">
                                <textarea rows="5" class="form-control media-info-input" data-id="%%id%%" data-attr="description" placeholder="Opis">%%description%%</textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 col-form-label">Link</label>
                            <div class="col-sm-8">
                                <input class="form-control media-url-info" readonly value="%%src%%"></input>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-4"></div>
                            <div class="col-sm-8">
                                <button type="button" data-id="%%id%%" class="btn btn-success btn-save">Zapisz</button>
                                <a href="%%src%%" target="_blank" class="btn btn-primary">Otwórz</a>
                                <a href="%%src%%" download="%%id%%" class="btn btn-success">
                                    <i class="mdi mdi-download"></i> Download
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
`
