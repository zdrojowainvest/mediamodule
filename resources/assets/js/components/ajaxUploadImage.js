export default (image) => {
    let data = new FormData();
    data.append('file', image);

    const id = ID();

    createThumbnail(id);

    const thumbnail = $(`#${id}`);
    $.ajax({
        progress: function (e) {
            if (e.lengthComputable) {
                const percentage = (e.loaded / e.total) * 100;

                thumbnail.find('.thumbnail__bar').animate({
                    width: percentage + '%'
                });
            }
        },
        type: 'POST',
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: window['ajaxUpload'],
        processData: false,
        data: data,
        enctype: 'multipart/form-data',
        contentType: false,
        success: function (data) {
            thumbnail.removeClass('thumbnail--loading');
            thumbnail.data('media-id', data.id);

            if(image.type === 'application/pdf') {
                thumbnail.html('<i class="mdi mdi-file-pdf-outline"></i>');
                return;
            }

            if(image.type.startsWith('image/')) {
                thumbnail.html(`<img class="thumbnail__image" src="${data.src}" alt="">`);
                return;
            }

            if(image.type.startsWith('video/')) {
                thumbnail.html('<i class="mdi mdi-file-video-outline"></i>');
                return;
            }

            thumbnail.html('<i class="mdi mdi-file-outline"></i>');
        },
        error: function () {
            thumbnail.remove();
            Swal.fire({
                type: 'error',
                title: 'Błąd',
                text: 'Wystąpił błąd serwera, spróbuj później.',
            });
        }
    });
}

function createThumbnail(id) {
    $(".media").prepend(thumbNailTemplate.split('%%id%%').join(id));
}

const thumbNailTemplate = `
    <div class="thumbnail thumbnail--loading" id="%%id%%">
        <div class="thumbnail__preload">
            <div class="thumbnail__bar" style="width: 10%"></div>
        </div>
    </div>
`;

/**
 * @return {string}
 */
const ID = function () {
    return `_${Math.random().toString(36).substr(2, 9)}`;
};
