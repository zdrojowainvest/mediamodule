export default `
    <div class='imageEditor__items'>
        <div class="imageEditor__toolbar-item imageEditor__cropTool">
            <i class="mdi mdi-scissors-cutting"></i>
            <span>Narzędzie przycinanie</span>
        </div>
        <div class="imageEditor__toolbar-item imageEditor__resizeTool">
            <i class="mdi mdi-move-resize"></i>
            <span>Zmiana rozmiaru</span>
        </div>
        
        <div class="imageEditor__toolbar-item imageEditor__exit" style="margin-left: auto; margin-right: 10px">
            <i class="mdi mdi-close-box-outline"></i>
            <span>Wyjdź z edytora</span>
        </div>
    </div>
`
