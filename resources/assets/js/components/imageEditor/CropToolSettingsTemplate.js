export default `
<div class="form-group">
    <label for="">Proporcje</label>
    
    <div class="row">
        <div class="col-md-6">
            <input type="text" class="form-control crop-tool-aspect-width">
        </div>
        <div class="col-md-6">
            <input type="text" class="form-control crop-tool-aspect-height">
        </div>
    </div>
</div>
<div class="form-group">
    <label for="">Rozmiary</label>
    
    <div class="row">
        <div class="col-md-6">
            <input type="text" class="form-control crop-tool-width">
        </div>
        <div class="col-md-6">
            <input type="text" class="form-control crop-tool-height">
        </div>
    </div>
</div>
<div class="form-group">
    <label for="">Oryginalny rozmiar</label>
    
    <div class="row">
        <div class="col-md-6">
            <input type="text" class="form-control crop-tool-natural-width" value="" disabled>
        </div>
        <div class="col-md-6">
            <input type="text" class="form-control crop-tool-natural-height" value="" disabled>
        </div>
    </div>
</div>
<div class="form-group">
    <button class="btn btn-sm btn-primary float-right crop-tool-append">Przytnij</button>
</div>
`;
