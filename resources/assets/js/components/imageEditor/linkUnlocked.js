export default `
<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<svg
   xmlns:dc="http://purl.org/dc/elements/1.1/"
   xmlns:cc="http://creativecommons.org/ns#"
   xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
   xmlns:svg="http://www.w3.org/2000/svg"
   xmlns="http://www.w3.org/2000/svg"
   xmlns:sodipodi="http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd"
   xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape"
   version="1.1"
   width="24"
   height="24"
   viewBox="0 0 24 24"
   id="svg4"
   sodipodi:docname="link-lock.svg"
   inkscape:version="0.92.3 (2405546, 2018-03-11)">
  <metadata
     id="metadata10">
    <rdf:RDF>
      <cc:Work
         rdf:about="">
        <dc:format>image/svg+xml</dc:format>
        <dc:type
           rdf:resource="http://purl.org/dc/dcmitype/StillImage" />
        <dc:title></dc:title>
      </cc:Work>
    </rdf:RDF>
  </metadata>
  <defs
     id="defs8" />
  <sodipodi:namedview
     pagecolor="#ffffff"
     bordercolor="#666666"
     borderopacity="1"
     objecttolerance="10"
     gridtolerance="10"
     guidetolerance="10"
     inkscape:pageopacity="0"
     inkscape:pageshadow="2"
     inkscape:window-width="2560"
     inkscape:window-height="1384"
     id="namedview6"
     showgrid="false"
     inkscape:zoom="19.666667"
     inkscape:cx="20.140474"
     inkscape:cy="4.7236698"
     inkscape:window-x="0"
     inkscape:window-y="30"
     inkscape:window-maximized="1"
     inkscape:current-layer="svg4" />
  <path
     d="m 17,17 v 4 M 7,8.9 h 4 V 7 H 7 C 0.33333623,7 0.33333623,17 7,17 h 4 V 15.1 H 7 C 2.8666685,15.1 2.8666685,8.8999999 7,8.9 M 8,11 v 2 h 8 v -2 m 1,-4 h -4 v 1.9 h 4 c 1.329777,-0.00554 2.513963,0.840306 2.94,2.1 0.186527,-0.01022 0.373473,-0.01022 0.56,0 h 1.4 C 21.424731,8.6716687 19.376343,6.999515 17,7 Z"
     id="path2"
     inkscape:connector-curvature="0"
     sodipodi:nodetypes="cccccssccsccccccccccccc" />
  <path
     d="m 17,17 h -4 v -1.9 h 4 c 1.329777,0.0055 2.513963,-0.840306 2.94,-2.1 0.186527,0.01022 0.373473,0.01022 0.56,0 h 1.4 c -0.475269,2.328331 -2.523657,4.000485 -4.9,4 z"
     id="path2-6"
     inkscape:connector-curvature="0"
     sodipodi:nodetypes="cccccccc" />
</svg>
`
