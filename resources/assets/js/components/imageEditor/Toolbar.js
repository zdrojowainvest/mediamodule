import ImageEditorToolbarTemplate from "./ImageEditorToolbarTemplate";
//import Cropper from 'cropperjs';
import 'cropperjs/dist/cropper.css';
import CropToolSettingsTemplate from "./CropToolSettingsTemplate";
import ResizeToolSettingsTemplate from "./ResizeToolSettingsTemplate";
import linkUnlocked from "./linkUnlocked";
import linkLocked from "./linkLocked";
import md5 from "./md5";
import crop from "tui-image-editor/src/js/ui/template/submenu/crop";

export default class Toolbar {

    constructor(id, editor) {
        this._editorId = id;
        this._active = null;
        this.editor = editor;
    }

    getTemplate() {
        return ImageEditorToolbarTemplate;
    }

    appendEvents() {
        this.appendCropEvents();
        this.appendResizeEvents();

        document.querySelector('.imageEditor__exit').addEventListener('click', () => {
            this.editor.close();
        })
    }

    appendCropEvents() {
        document.querySelector('.imageEditor__cropTool').addEventListener('click', () => {
            if (this._active !== 'crop') {
                this.setActive('crop');
                let aspectRatio = 0;

                document.querySelector('.imageEditor__tool-options').innerHTML = CropToolSettingsTemplate;

                const imageElement =  $(".imageEditor__image-element")[0];

                const selectChange = (img, selection) => {
                    $(".crop-tool-width").val(selection.width);
                    $(".crop-tool-height").val(selection.height);
                };

                const initialize = (img, selection) => {
                    $(".crop-tool-width").val(selection.width);
                    $(".crop-tool-height").val(selection.height);
                    $(".crop-tool-natural-height").val(imageElement.naturalHeight);
                    $(".crop-tool-natural-width").val(imageElement.naturalWidth);
                };


                window['cropToolInstance'] = $(".imageEditor__image-element").imgAreaSelect({
                    imageHeight: imageElement.naturalHeight,
                    imageWidth: imageElement.naturalWidth,
                    x1: 30,
                    x2: imageElement.naturalWidth - 30,
                    y1: 30,
                    y2: imageElement.naturalHeight - 30,
                    instance: true,
                    handles: true,
                    persistent: true,

                    onInit: initialize,
                    onSelectChange: selectChange
                });

                document.querySelector('.crop-tool-aspect-width').addEventListener('keyup', function () {


                    if (this.value === '') {
                        cropToolInstance.setOptions({
                            aspectRatio: '',
                        });

                        return;
                    }


                    if (!document.querySelector('.crop-tool-aspect-height').value) {
                        return;
                    }

                    if (this.value / document.querySelector('.crop-tool-aspect-height').value > 50) {
                        this.value = '';
                        cropToolInstance.setOptions({
                            aspectRatio: '',
                        });
                        return;
                    }

                    cropToolInstance.setOptions({
                        aspectRatio: this.value + ":" +  document.querySelector('.crop-tool-aspect-height').value,
                    });
                    aspectRatio = this.value / document.querySelector('.crop-tool-aspect-height').value;
                });

                document.querySelector('.crop-tool-aspect-height').addEventListener('keyup', function () {
                    if (this.value === '') {
                        cropToolInstance.setOptions({
                            aspectRatio: '',
                        });

                        return;
                    }



                    if (!document.querySelector('.crop-tool-aspect-width').value) {
                        return;
                    }

                    if (document.querySelector('.crop-tool-aspect-width').value / this.value > 50) {
                        this.value = '';

                        cropToolInstance.setOptions({
                            aspectRatio: '',
                        });

                        return;
                    }

                    cropToolInstance.setOptions({
                        aspectRatio: document.querySelector('.crop-tool-aspect-width').value + ':' +  this.value,
                    });

                    aspectRatio = document.querySelector('.crop-tool-aspect-width').value / this.value;
                });

                let imageData = null;

                setInputFilter(document.querySelector('.crop-tool-aspect-height'), function (value) {
                    if(value === '') return true;

                    return /^[0-9]+$/.test(value);
                });
                setInputFilter(document.querySelector('.crop-tool-aspect-width'), function (value) {
                    if(value === '') return true;

                    return /^[0-9]+$/.test(value);
                });

                document.querySelector('.crop-tool-width').addEventListener('keyup', function () {
                    if (this.value > imageElement.naturalWidth) {
                        this.value = imageElement.naturalWidth;
                    }

                    const selection = cropToolInstance.getSelection();

                    let {y1, y2} = selection;

                    if(aspectRatio > 0) {
                        const resize = resizeHeight(this.value / aspectRatio);
                        y1 = resize.y1;
                        y2 = resize.y2;
                    }



                    if(parseInt(this.value) + selection.x1 > imageElement.naturalWidth) {
                        const x1 = (parseInt(this.value) + selection.x1) - imageElement.naturalWidth - selection.x1;

                        cropToolInstance.setSelection(x1, y1, this.value, y2);
                        cropToolInstance.update();

                        return;
                    }

                    $(".crop-tool-height").val(Math.ceil(y2));
                    cropToolInstance.setSelection(selection.x1, y1, this.value, y2);
                    cropToolInstance.update();
                });

                function resizeHeight(height) {
                    if (height > imageElement.naturalHeight) {
                        height = imageElement.naturalHeight;
                    }

                    const selection = cropToolInstance.getSelection();

                    if(parseInt(height) + selection.y1 > imageElement.naturalHeight) {
                        const y1 = (parseInt(height) + selection.y1) - imageElement.naturalHeight - selection.y1;

                        return {
                            y1: y1,
                            y2: height
                        };
                    }

                    return {
                        y1: selection.y1,
                        y2: height
                    };
                }

                function resizeWidth(width) {
                    if (width > imageElement.naturalWidth) {
                        width = imageElement.naturalWidth;
                    }

                    const selection = cropToolInstance.getSelection();


                    if(parseInt(width) + selection.x1 > imageElement.naturalWidth) {
                        const x1 = (parseInt(width) + selection.x1) - imageElement.naturalWidth - selection.x1;

                        return {
                            x1: x1,
                            x2: width
                        }
                    }

                    return {
                        x1: selection.x1,
                        x2: width
                    };
                }

                document.querySelector('.crop-tool-height').addEventListener('keyup', function () {

                    if (this.value > imageElement.naturalHeight) {
                        this.value = imageElement.naturalHeight;
                    }

                    const selection = cropToolInstance.getSelection();

                    let {x1, x2} = selection;

                    if(aspectRatio > 0) {
                        const resize = resizeHeight(aspectRatio * this.value);
                        x1 = resize.x1;
                        x2 = resize.x2;
                    }

                    if(parseInt(this.value) + selection.y1 > imageElement.naturalHeight) {
                        const y1 = (parseInt(this.value) + selection.y1) - imageElement.naturalHeight - selection.y1;

                        cropToolInstance.setSelection(x1, y1, x2, this.value);
                        cropToolInstance.update();

                        return;
                    }

                    cropToolInstance.setSelection(x1, selection.y1, x2, this.value);
                    cropToolInstance.update();
                });

                document.querySelector('.crop-tool-append').addEventListener('click', () => {
                    const data = cropToolInstance.getSelection();


                    const history = {
                        x: data.x1,
                        y: data.y1,
                        cH: data.height,
                        cW: data.width
                    };

                    this.editor._history.push(this.mergeHistory(history));

                    this.setActive(null);
                    this.clearSettings();

                    $(".imageEditor__image-element").imgAreaSelect({remove: true});

                    document.querySelector('.imageEditor__preview').querySelector('img').setAttribute('src', this.makeUrl(this.editor._history[this.editor._history.length - 1]));
                });
            }
            else {
                this.setActive(null);
                this.clearSettings();

                $(".imageEditor__image-element").imgAreaSelect({remove: true});

            }
        });
    }

    appendResizeEvents() {
        document.querySelector('.imageEditor__resizeTool').addEventListener('click', () => {
            if(this._active !== 'resize') {
                $(".imageEditor__image-element").imgAreaSelect({remove: true});

                this.setActive('resize');
                document.querySelector('.imageEditor__tool-options').innerHTML = ResizeToolSettingsTemplate;

                const imageElement = document.querySelector('.imageEditor__preview img');

                let lockedAspectRatio = true;
                let aspectRatio = imageElement.naturalWidth / imageElement.naturalHeight;

                document.querySelector('.resize-tool-width').value = imageElement.naturalWidth;
                document.querySelector('.resize-tool-height').value = Math.ceil(imageElement.naturalWidth / aspectRatio);

                setInputFilter(document.querySelector('.resize-tool-width'), function (value) {
                    return /^[0-9]+$/.test(value);
                });

                setInputFilter(document.querySelector('.resize-tool-height'), function (value) {
                    return /^[0-9]+$/.test(value);
                });

                document.querySelector('.resize-tool-width').addEventListener('keyup', function () {
                    if (this.value > imageElement.naturalWidth) {
                        this.value = imageElement.naturalWidth;
                    }

                    if(lockedAspectRatio) {
                        document.querySelector('.resize-tool-height').value = Math.ceil(this.value / aspectRatio);
                    }
                });

                document.querySelector('.resize-tool-height').addEventListener('keyup', function () {
                    if (this.value > imageElement.naturalHeight) {
                        this.value = imageElement.naturalHeight;
                    }

                    if(lockedAspectRatio) {
                        document.querySelector('.resize-tool-width').value = Math.ceil(this.value * aspectRatio);
                    }
                });

                document.querySelector('.resize-change-aspect-ratio').addEventListener('click', function () {
                    if(lockedAspectRatio) {
                        this.innerHTML = linkUnlocked;
                        lockedAspectRatio = false;
                    }
                    else {
                        this.innerHTML = linkLocked;
                        lockedAspectRatio = true;
                        document.querySelector('.resize-tool-height').value = Math.ceil(document.querySelector('.resize-tool-width').value / aspectRatio);
                    }
                });

                document.querySelector('.resize-tool-append').addEventListener('click', () => {
                    const history = {
                        h: document.querySelector('.resize-tool-height').value,
                        w: document.querySelector('.resize-tool-width').value,
                    };

                    this.editor._history.push(this.mergeHistory(history));

                    this.setActive(null);
                    this.clearSettings();

                    $(".imageEditor__image-element").imgAreaSelect({remove: true});

                    document.querySelector('.imageEditor__preview').querySelector('img').setAttribute('src', this.makeUrl(this.editor._history[this.editor._history.length - 1]));
                });
            }
            else {
                this.setActive(null);
                this.clearSettings();
            }
        })
    }

    clearSettings() {
        if(document.querySelector('.imageEditor__tool-options')) {
            document.querySelector('.imageEditor__tool-options').innerHTML = "";
        }
    }

    setActive(active) {
        this._active = active;
    }

    getPreview() {
        return document.querySelector(`#imageEditor__${this._editorId} .imageEditor__preview`);
    }

    makeUrl() {
        return window['imageEditorUrl'] + '/edit?' + this.makeUrlFromHistory()
    }

    makeUrlFromHistory() {
        const params = new URLSearchParams();
        params.append('history', btoa(JSON.stringify(this.editor._history)));

        return params;
    }

    mergeHistory(history) {
        const lastHistory = this.editor._history[this.editor._history.length - 1];

        if(history.x < 0) history.x = 0;

        const historyToPush = {};

        if(history.x) historyToPush.x = history.x;
        if(history.y) historyToPush.y = history.y;
        if(history.h) historyToPush.h = history.h;
        if(history.w) historyToPush.w = history.w;
        if(history.cW) historyToPush.cW = history.cW;
        if(history.cH) historyToPush.cH = history.cH;

        if(!lastHistory) {
            historyToPush.s = history.s || this.editor._originalImage;

            return historyToPush;
        }


        historyToPush.s = history.s || lastHistory.source;

        return historyToPush;
    }
}


function setInputFilter(textbox, inputFilter) {
    ["input", "keydown", "keyup", "mousedown", "mouseup", "select", "contextmenu", "drop"].forEach(function (event) {
        textbox.addEventListener(event, function () {
            if (inputFilter(this.value)) {
                this.oldValue = this.value;
                this.oldSelectionStart = this.selectionStart;
                this.oldSelectionEnd = this.selectionEnd;
            } else if (this.hasOwnProperty("oldValue")) {
                this.value = this.oldValue;
                this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
            } else {
                this.value = "";
            }
        });
    });
}

var getDataUrl = function (img) {
    var canvas = document.createElement('canvas')
    var ctx = canvas.getContext('2d')

    canvas.width = img.naturalWidth;
    canvas.height = img.naturalHeight;
    ctx.drawImage(img, 0, 0)

    // If the image is not png, the format
    // must be specified here
    return canvas.toDataURL()
}

function dataURItoBlob(dataURI) {
    // convert base64/URLEncoded data component to raw binary data held in a string
    var byteString;
    if (dataURI.split(',')[0].indexOf('base64') >= 0)
        byteString = atob(dataURI.split(',')[1]);
    else
        byteString = unescape(dataURI.split(',')[1]);

    // separate out the mime component
    var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];

    // write the bytes of the string to a typed array
    var ia = new Uint8Array(byteString.length);
    for (var i = 0; i < byteString.length; i++) {
        ia[i] = byteString.charCodeAt(i);
    }

    return new Blob([ia], {type:mimeString});
}

