$.fn.mediaSelector = function (options) {
    const settings = {
        extensions: 'all'
    };


    return this.each(function() {
        if(options) $.extend(settings, options);





        $(this).click(function () {
            openSelectModal(settings.extensions, $(this));
        });



    });
};

function openSelectModal(extensions, object) {
    $("body").append(`
        <div class='select-modal__overlay' data-extensions="${extensions}">
            <div class="select-modal__content">
                <div class="select-modal__header">
                    <div class="select-modal__input">
                        <input type="text" id="select-modal-searcher" class="form-control" placeholder="Wyszukaj po tytulu">
                    </div>
                    <div class="select-modal__close">
                        <i class="mdi mdi-close"></i>
                    </div>
                </div>
                <div class="select-modal__files">
                
                </div>
            </div>
        </div>
    `);


    appendPhotos(extensions);

    $("#select-modal-searcher").on("keyup", function() {
        if($(this).val() !== '') {
            $(".select-modal__thumbnail").hide();
            $(`.select-modal__thumbnail[title*="${$(this).val()}"]`).show();
        }
        else {
            $(".select-modal__thumbnail").show();
        }
    })

    $(".select-modal__close").click(function() {
       closeSelect();
    });
    $("body").on("click", ".select-modal__thumbnail", function () {

        $.ajax({
            url: `/dashboard/media/${$(this).data('id')}/info`,
            success: async function (res) {

                let options = {
                    default: 'Domyślny'
                };

                if(res.mimetype.startsWith('image') && res.extension !== 'svg') {
                    Object.keys(res.types).forEach((type) => {
                        options[type] = type
                    });


                    const {value: version} = await Swal.fire({
                        title: 'Wybierz wersje obrazu',
                        input: 'select',
                        showCancelButton: true,
                        inputOptions: options
                    });


                    if (version) {
                        if (version === 'default') {
                            object.trigger('media:selected', [
                                {
                                    _id: res._id,
                                    alt: res.alt,
                                    title: res.title,
                                    url: `/media/${res._id}`
                                }
                            ])

                            closeSelect();
                        } else {
                            object.trigger('media:selected', [
                                {
                                    _id: res._id,
                                    alt: res.alt,
                                    title: res.title,
                                    url: `/media/${res._id}-${version}`
                                }
                            ])
                            closeSelect();

                        }

                    }
                } else {
                    const {value: select} = await Swal.fire({
                        title: 'Potwierdź wybranie',
                        showCancelButton: true,
                    });

                    if(select) {
                        object.trigger('media:selected', [
                            {
                                _id: res._id,
                                alt: res.alt,
                                title: res.title,
                                url: `/media/${res._id}`
                            }
                        ])

                        closeSelect();
                    }
                }
            }
        });
    });
}

function debounce(func, wait, immediate) {
    var timeout;
    return function() {
        var context = this, args = arguments;
        var later = function() {
            timeout = null;
            if (!immediate) func.apply(context, args);
        };
        var callNow = immediate && !timeout;
        clearTimeout(timeout);
        timeout = setTimeout(later, wait);
        if (callNow) func.apply(context, args);
    };
}

function closeSelect() {
    $('.select-modal__overlay').remove();
}

function appendPhotos(extensions) {
    $(".select-modal__files").html("");

    $.ajax({
        url: `/dashboard/media/selector/ajax?extensions=${extensions}`,
        method: 'GET',
        success: function (response) {
            response.forEach((element) => {
                if(element.mimetype.startsWith('image/')) {
                    $('.select-modal__files').append(`
                    <div class="select-modal__thumbnail" data-id="${element._id}" title="${element.title}">
                        <img class="select-modal__thumbnail-image" data-src="/media/${element._id}-dashboardthumbnail"/>
                        <span>${element.title}</span>
                        <span>${element.created_at}</span>
                    </div>
                `)
                }
                else {

                    let type = 'mdi mdi-file-outline';
                    if (element.extension === 'pdf') {
                        type = 'mdi mdi-file-pdf-outline'
                    }
                    if (element.mimetype.startsWith('video/')) {
                        type = 'mdi mdi-file-video-outline';
                    }
                    $('.select-modal__files').append(`
                    <div class="select-modal__thumbnail" data-id="${element._id}" title="${element.title}">
                        <i class="${type}"></i>
                        <span>${element.title}</span>
                        <span>${element.created_at}</span>
                    </div>
                `)
                }
            });

            var lazyLoadInstance = new LazyLoad({
                elements_selector: ".select-modal__thumbnail-image"
                // ... more custom settings?
            });
        }
    })
}
