import Toolbar from './Toolbar';
import ImageEditorTemplate from "./ImageEditorTemplate";

export default class ImageEditor {

    constructor(selector, options) {
        this._selector = selector;
        this._options = options;
        this._idNumber = ID();
        this._toolbar = new Toolbar(this._idNumber, this);
        this._object = document.querySelector(selector);
        this._originalImage = options.image;
        this._baseImage = options.image;
        this._history = [];
        this._saving = false;
        this.renderEditor();
        this.appendEvents();
    }

    close() {
        this._toolbar.setActive(null);
        this._toolbar.clearSettings();
        $(".imageEditor__image-element").imgAreaSelect({remove: true});
        if(this._object) {
            this._object.innerHTML = '';
            this._object.style.display = 'none';
        }

        delete this;
    }

    appendEvents() {
        document.querySelector('.imageEditor__save-button').addEventListener('click', () => {
            if( document.querySelector('.imageEditor__save-input-error')) {
                document.querySelector('.imageEditor__save-input-error').remove();
            }

            const name = document.querySelector('.imageEditor__name-input').value;

            if(name === '' || name.length > 20) {
                const element = document.createElement('small');
                element.classList.add('error');
                element.classList.add('imageEditor__save-input-error');
                element.innerHTML = "Nazwa jest nieprawidłowa";


                document.querySelector('.imageEditor__save-input-container').append(element);

                return;
            }


            if( document.querySelector('.imageEditor__save-input-error')) {
                document.querySelector('.imageEditor__save-input-error').remove();
            }

            const selectNames = document.querySelector('.image-chooser');
            for(let i = 0; i < selectNames.options.length; i++) {
                if(name == selectNames.options[i].text) {
                    const element = document.createElement('small');
                    element.classList.add('error');
                    element.classList.add('imageEditor__save-input-error');
                    element.innerHTML = "Nazwa musi być unikalna";


                    document.querySelector('.imageEditor__save-input-container').append(element);

                    return;
                }
            }

            const editor = this;



            if(!this._saving) {
                this._saving = true;
                fetch($(".imageEditor__image-element").attr('src')).then(res => res.blob()).then((blob) => {
                    const formData = new FormData();
                    formData.append('_token', window['csrf']);
                    formData.append('image', blob);
                    formData.append('media', this._options.mediaId);
                    formData.append('name', name);

                    $.ajax({
                        method: 'POST',
                        url: window['imageEditSaveUrl'],
                        data: formData,
                        processData: false,
                        contentType: false,
                        success: function () {
                            const data = {
                                id: window['imageUrlType'].split('%%id%%').join(editor._options.mediaId).split('%%type%%').join(name),
                                text: name
                            };

                            const newOption = new Option(data.text, data.id, false, false);
                            $('.image-chooser').append(newOption).trigger('change');

                            editor.close();
                        },
                        error: function() {
                            this._saving = false;
                            Swal.fire({
                                type: 'error',
                                title: 'Błąd',
                                text: 'Wystąpił błąd serwera, spróbuj później.',
                            });
                        }


                    });
                }).catch(() => {
                    this._saving = false;
                    Swal.fire({
                        type: 'error',
                        title: 'Błąd',
                        text: 'Wystąpił błąd serwera, spróbuj później.',
                    });
                });
            }


        })
    }

    getTemplate() {
        return ImageEditorTemplate
            .split('%%id%%')
            .join(this._idNumber)
            .split('%%preview%%')
            .join(this._baseImage)
            .split('%%toolbar%%')
            .join(this._toolbar.getTemplate())
            .split('%%static%%')
            .join(document.querySelector('.info__static').outerHTML)

    }

    renderEditor() {
        this._object.innerHTML = this.getTemplate();
        this._toolbar.appendEvents();
    }





}

/**
 * @return {string}
 */
const ID = function () {
    return `_${Math.random().toString(36).substr(2, 9)}`;
};

