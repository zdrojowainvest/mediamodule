export default `
    <div id="imageEditor__%%id%%" class="imageEditor">
        <div class="imageEditor__left">
                %%toolbar%%
        
        <div class="imageEditor__content">
            <div class="imageEditor__preview">
                <img src="%%preview%%" class="imageEditor__image-element">
            </div>
        </div>  
        </div>
        <div class="imageEditor__right">
            %%static%%
            
            <div class="imageEditor__tool-options"></div>
            
            <div class="imageEditor__save">
                
                                <div class="col-8 ml-auto mr-auto text-center imageEditor__save-input-container">
                <label for="">Nazwa wersji obrazka</label>
                    <input type="text" class="form-control imageEditor__name-input" maxlength="20">
                </div>
                
                <div class="col-8 ml-auto mr-auto text-center">
                    <button class="btn btn-primary mt-3 imageEditor__save-button btn-lg" style="width: 100%">Zapisz</button>
                </div>
                
            </div>
        </div>  
    </div>
`;
