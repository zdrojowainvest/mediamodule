require('./components/imageEditor/imageAreaSelect');
require('./components/imageEditor/jQueryPlugin');

(function($, window, undefined) {
    //is onprogress supported by browser?
    var hasOnProgress = ("onprogress" in $.ajaxSettings.xhr());

    //If not supported, do nothing
    if (!hasOnProgress) {
        return;
    }

    //patch ajax settings to call a progress callback
    var oldXHR = $.ajaxSettings.xhr;
    $.ajaxSettings.xhr = function() {
        var xhr = oldXHR.apply(this, arguments);
        if(xhr instanceof window.XMLHttpRequest) {
            xhr.addEventListener('progress', this.progress, false);
        }

        if(xhr.upload) {
            xhr.upload.addEventListener('progress', this.progress, false);
        }

        return xhr;
    };
})(jQuery, window);

$(".thumbnail").on('mousedown', function (e) {
    e.preventDefault();
    e.stopPropagation();
});

window.addEventListener("dragover",function(e){
    e = e || event;
    e.preventDefault();
},false);
window.addEventListener("drop",function(e){
    e = e || event;
    e.preventDefault();
},false);

const dropArea = $(".drop");
const dropFix = $(".drop__fix");

window['canDrop'] = true;
window['dropping'] = false;

$(document).on('dragover dragenter', function(e) {
    if(canDrop) {
        dropArea.addClass('drop--visible');
        dropping = true;
    }
});

dropFix.on('dragend dragleave drop', function() {
    dropArea.removeClass('drop--visible');

});

dropFix.on('drop', function (e) {
    if(dropping) {
        e.preventDefault();

        const droppedFiles = e.originalEvent.dataTransfer.files;

        for (let i = 0; i < droppedFiles.length; i++) {
            ajaxUploadImage(droppedFiles.item(i));
        }
    }
});


import ajaxUploadImage from "./components/ajaxUploadImage";
import openModal from './components/modal';

$("body").on('click', '.thumbnail', function () {
    if($(this).hasClass('thumbnail--loading')) return;

    openModal($(this).data('media-id'));
});

$(".media-id-searcher").on('keyup', debounce(function () {
    const search = $(this).val();

    $(".thumbnail").each(function () {
        if($(this).data('media-id').indexOf(search) > -1) $(this).show();
        else $(this).hide();
    })
}, 300));

function debounce(func, wait, immediate) {
    var timeout;
    return function() {
        var context = this, args = arguments;
        var later = function() {
            timeout = null;
            if (!immediate) func.apply(context, args);
        };
        var callNow = immediate && !timeout;
        clearTimeout(timeout);
        timeout = setTimeout(later, wait);
        if (callNow) func.apply(context, args);
    };
}

require('./components/editImage');

